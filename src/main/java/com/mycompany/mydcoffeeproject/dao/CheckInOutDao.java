/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.dao;

import com.mycompany.mydcoffeeproject.helper.DatabaseHelper;
import com.mycompany.mydcoffeeproject.model.CheckInOut;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pasinee
 */
public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
//    public CheckInOut get(int id) {
//        CheckInOut CheckInOut = null;
//        String sql = "SELECT * FROM CHECKINOUT WHERE check_in_out_id=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, id);
//            ResultSet rs = stmt.executeQuery();
//            while (rs.next()) {
//                CheckInOut = CheckInOut.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return CheckInOut;
//    }
    
    public CheckInOut get(int id) {
    CheckInOut checkInOut = null;
    String sql = "SELECT * FROM CHECKINOUT WHERE check_in_out_id=?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            checkInOut = CheckInOut.fromRS(rs);
        }
        if (checkInOut != null) {
            // Ensure that the check date is not null before accessing it
            if (checkInOut.getCheckDate() != null) {
                // Perform the necessary operations here
            } else {
                // Handle the case where the check date is null
            }
        } else {
            // Handle the case where CheckInOut is null
        }

    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return checkInOut;
}


    @Override
    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CHECKINOUT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
//                checkInOut.setId(rs.getInt("check_in_out_id"));
//                checkInOut.setEmpId(rs.getInt("emp_id"));
//                checkInOut.setCheckDate(rs.getTimestamp("check_date"));
//                checkInOut.setCheckIn(rs.getString("check_in"));
//                checkInOut.setCheckOut(rs.getString("check_out"));
//                checkInOut.setCheckStatus(rs.getString("check_status"));
//                checkInOut.setTotalHr(rs.getFloat("total_hr"));
                
                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckInOut> getAll(String where, String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CHECKINOUT where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAll(String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CHECKINOUT  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {

        String sql = "INSERT INTO CHECKINOUT ( check_in_out_id, emp_id, check_date, check_in, check_out, check_status, total_hr)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getEmpId());
            stmt.setLong(3, obj.getCheckDate().getTime());
            stmt.setString(4, obj.getCheckIn());
            stmt.setString(5, obj.getCheckOut());
            stmt.setString(6, obj.getCheckStatus());
            stmt.setFloat(7, obj.getTotalHr());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null; 
        }
        return obj;
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE CHECKINOUT"
                + " SET emp_id = ?, check_date = ?,check_in = ?, check_out = ?, check_status = ?, total_hr = ?"
                + " WHERE check_in_out_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getEmpId());
            stmt.setLong(3, obj.getCheckDate().getTime());
            stmt.setString(4, obj.getCheckIn());
            stmt.setString(5, obj.getCheckOut());
            stmt.setString(6, obj.getCheckStatus());
            stmt.setFloat(7, obj.getTotalHr());
            

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM CHECKINOUT WHERE check_in_out_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    

}
