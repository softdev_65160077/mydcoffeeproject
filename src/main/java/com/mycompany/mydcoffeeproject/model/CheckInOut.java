/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pasinee
 */
public class CheckInOut {

    private int id;
    private int empId;
    private java.util.Date checkDate;
    private String checkIn;
    private String checkOut;
    private String checkStatus;
    private float totalHr;

    public CheckInOut(int id, int empId, Date checkDate, String checkIn, String checkOut, String checkStatus, float totalHr) {
        this.id = id;
        this.empId = empId;
        this.checkDate = checkDate;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.checkStatus = checkStatus;
        this.totalHr = totalHr;
    }

    public CheckInOut(int empId, Date checkDate, String checkIn, String checkOut, String checkStatus, float totalHr) {
        this.id = -1;
        this.empId = empId;
        this.checkDate = checkDate;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.checkStatus = checkStatus;
        this.totalHr = totalHr;
    }

    public CheckInOut() {
        this.empId = -1;
        this.checkDate = null;
        this.checkIn = "";
        this.checkOut = "";
        this.checkStatus = "";
        this.totalHr = 0.0f;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public float getTotalHr() {
        return totalHr;
    }

    public void setTotalHr(float totalHr) {
        this.totalHr = totalHr;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", empId=" + empId + ", checkDate=" + checkDate + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", checkStatus=" + checkStatus + ", totalHr=" + totalHr + '}';
    }

    
    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("check_in_out_id"));
            checkInOut.setEmpId(rs.getInt("emp_id"));
            checkInOut.setCheckDate(rs.getTimestamp("check_date"));
            checkInOut.setCheckIn(rs.getString("check_in"));
            checkInOut.setCheckOut(rs.getString("check_out"));
            checkInOut.setCheckStatus(rs.getString("check_status"));
            checkInOut.setTotalHr(rs.getFloat("total_hr"));

        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkInOut;
    }

    
}
